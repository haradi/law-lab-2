from fastapi import FastAPI, Form
from pydantic import BaseModel

app = FastAPI()

class LCS(BaseModel):
    str1: str
    str2: str

# source : https://www.programiz.com/dsa/longest-common-subsequence
def lcs_algo(S1, S2):
    m = len(S1)
    n = len(S2)
    L = [[0 for x in range(n+1)] for x in range(m+1)]

    # Building the mtrix in bottom-up way
    for i in range(m+1):
        for j in range(n+1):
            if i == 0 or j == 0:
                L[i][j] = 0
            elif S1[i-1] == S2[j-1]:
                L[i][j] = L[i-1][j-1] + 1
            else:
                L[i][j] = max(L[i-1][j], L[i][j-1])

    index = L[m][n]

    lcs_algo = [""] * (index+1)
    lcs_algo[index] = ""

    i = m
    j = n
    while i > 0 and j > 0:
        if S1[i-1] == S2[j-1]:
            lcs_algo[index-1] = S1[i-1]
            i -= 1
            j -= 1
            index -= 1
        elif L[i-1][j] > L[i][j-1]:
            i -= 1
        else:
            j -= 1
            
    return "".join(lcs_algo)

@app.get("/")
def read_root():
    return {"Test": "Create Webservice Lab 2"}

@app.get("/lcs")
def get_lcs(str1: str, str2: str):
    return {"String 1": str1, "String 2": str2, "LCS":lcs_algo(str1, str2)}

@app.post("/lcs/form")
def post_lcs_form(str1: str = Form(...), str2: str = Form(...)):
    return {"String 1": str1, "String 2": str2, "LCS":lcs_algo(str1, str2)}

@app.post("/lcs/json")
def post_lcs_json(data: LCS):
    return {"String 1": data.str1, "String 2": data.str2, "LCS":lcs_algo(data.str1, data.str2)}
    
